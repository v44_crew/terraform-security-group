# AWS EC2-VPC Security Group Terraform module

These types of resources are supported:

* [EC2-VPC Security Group](https://www.terraform.io/docs/providers/aws/r/security_group.html)
* [EC2-VPC Security Group Rule](https://www.terraform.io/docs/providers/aws/r/security_group_rule.html)

## Features

This module aims to implement **ALL** combinations of arguments supported by AWS
* IPv4/IPv6 CIDR blocks
* VPC endpoint prefix lists (use data source [aws_prefix_list]
* Access from source security groups
* Access from self
* Named rules ([see the rules here](https://bitbucket.org/v44_crew/terraform-security-group/src/master/rules.tf))
* Named groups of rules with ingress (inbound) and egress (outbound) ports open for common scenarios (eg, [ssh](https://bitbucket.org/v44_crew/terraform-security-group/src/master/modules/ssh), [http-80](https://bitbucket.org/v44_crew/terraform-security-group/src/master/modules/http-80), [mysql](https://bitbucket.org/v44_crew/terraform-security-group/src/master/modules/mysql), see the whole list [here](https://bitbucket.org/v44_crew/terraform-security-group/src/master/modules/README.md))
* Conditionally create security group and all required security group rules ("single boolean switch").

Ingress and egress rules can be configured in a variety of ways. See [inputs section](#inputs) for all supported arguments.

## Usage

There are two ways to create security groups using this module:

1. Specifying predefined rules (HTTP, SSH, etc)
1. Specifying custom rules

### Security group with predefined rules

```hcl
module "web_server_sg" {
  source = "git::https://bitbucket.org/v44_crew/terraform-security-group//modules/http-80.git?ref=tags/v1.0.0"

  name        = "web-server"
  description = "Security group for web-server with HTTP ports open within VPC"
  vpc_id      = "vpc-12345678"

  ingress_cidr_blocks = ["10.10.0.0/16"]
}
```

### Security group with custom rules

```hcl
module "vote_service_sg" {
  source = "git::https://bitbucket.org/v44_crew/terraform-security-group.git?ref=tags/v1.0.0"

  name        = "user-service"
  description = "Security group for user-service with custom ports open within VPC, and PostgreSQL publicly open"
  vpc_id      = "vpc-12345678"

  ingress_cidr_blocks      = ["10.10.0.0/16"]
  ingress_rules            = ["https-443-tcp"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 8080
      to_port     = 8090
      protocol    = "tcp"
      description = "User-service ports"
      cidr_blocks = "10.10.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}
```

Please note that the `ref` argument in the `source` URL should always be set to the latest version value. You can find the latest version by checking for the most recent tag in the repository.

### Note about "value of 'count' cannot be computed"

Terraform 0.11 has a limitation which does not allow **computed** values inside `count` attribute on resources (issues: [#16712](https://github.com/hashicorp/terraform/issues/16712), [#18015](https://github.com/hashicorp/terraform/issues/18015), ...)

Computed values are values provided as outputs from `module`. Non-computed values are all others - static values, values referenced as `variable` and from data-sources.

When you need to specify computed value inside security group rule argument you need to specify it using an argument which starts with `computed_` and provide a number of elements in the argument which starts with `number_of_computed_`. See these examples:

```hcl
module "http_sg" {
  source = "terraform-aws-modules/security-group/aws"
  # omitted for brevity
}

module "db_computed_source_sg" {
  # omitted for brevity

  vpc_id = "vpc-12345678" # these are valid values also - "${module.vpc.vpc_id}" and "${local.vpc_id}"

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "mysql-tcp"
      source_security_group_id = "${module.http_sg.this_security_group_id}"
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 1
}

module "db_computed_sg" {
  # omitted for brevity

  ingress_cidr_blocks = ["10.10.0.0/16", "${data.aws_security_group.default.id}"]

  computed_ingress_cidr_blocks = ["${module.vpc.vpc_cidr_block}"]
  number_of_computed_ingress_cidr_blocks = 1
}

module "db_computed_merged_sg" {
  # omitted for brevity

  computed_ingress_cidr_blocks = ["10.10.0.0/16", "${module.vpc.vpc_cidr_block}"]
  number_of_computed_ingress_cidr_blocks = 2
}
```

Note that `db_computed_sg` and `db_computed_merged_sg` are equal, because it is possible to put both computed and non-computed values in arguments starting with `computed_`.

## Conditional creation

Sometimes you need to have a way to create security group conditionally but Terraform does not allow to use `count` inside `module` block, so the solution is to specify argument `create`.

```hcl
# This security group will not be created
module "vote_service_sg" {
  source = "terraform-aws-modules/security-group/aws"

  create = false
  # ... omitted
}
```

## How to add/update rules/groups?

Rules and groups are defined in [rules.tf](https://bitbucket.org/v44_crew/terraform-security-group/src/master/rules.tf). Run `update_groups.sh` when content of that file has changed to recreate content of all automatic modules.

## Known issues

* Due to an [issue #1920](https://github.com/terraform-providers/terraform-provider-aws/issues/1920) in AWS provider, updates to the `description` of security group rules are ignored by this module. If you need to update `description` after the security group has been created you need to recreate security group rule.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| auto\_groups | Map of groups of security group rules to use to generate modules (see update_groups.sh) | map(map(list(string))) | `{ "carbon-relay-ng": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "carbon-line-in-tcp", "carbon-line-in-udp", "carbon-pickle-tcp", "carbon-pickle-udp", "carbon-gui-udp" ], "ingress_with_self": [ "all-all" ] } ], "cassandra": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "cassandra-clients-tcp", "cassandra-thrift-clients-tcp", "cassandra-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ], "consul": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "consul-tcp", "consul-cli-rpc-tcp", "consul-webui-tcp", "consul-dns-tcp", "consul-dns-udp", "consul-serf-lan-tcp", "consul-serf-lan-udp", "consul-serf-wan-tcp", "consul-serf-wan-udp" ], "ingress_with_self": [ "all-all" ] } ], "docker-swarm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "docker-swarm-mngmt-tcp", "docker-swarm-node-tcp", "docker-swarm-node-udp", "docker-swarm-overlay-udp" ], "ingress_with_self": [ "all-all" ] } ], "elasticsearch": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "elasticsearch-rest-tcp", "elasticsearch-java-tcp" ], "ingress_with_self": [ "all-all" ] } ], "http-80": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "http-80-tcp" ], "ingress_with_self": [ "all-all" ] } ], "https-443": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "https-443-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ipsec-4500": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ipsec-4500-udp" ], "ingress_with_self": [ "all-all" ] } ], "ipsec-500": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ipsec-500-udp" ], "ingress_with_self": [ "all-all" ] } ], "kafka": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "kafka-broker-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ldaps": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ldaps-tcp" ], "ingress_with_self": [ "all-all" ] } ], "memcached": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "memcached-tcp" ], "ingress_with_self": [ "all-all" ] } ], "mssql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "mssql-tcp", "mssql-udp", "mssql-analytics-tcp", "mssql-broker-tcp" ], "ingress_with_self": [ "all-all" ] } ], "mysql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "mysql-tcp" ], "ingress_with_self": [ "all-all" ] } ], "nfs": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "nfs-tcp" ], "ingress_with_self": [ "all-all" ] } ], "nomad": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "nomad-http-tcp", "nomad-rpc-tcp", "nomad-serf-tcp", "nomad-serf-udp" ], "ingress_with_self": [ "all-all" ] } ], "openvpn": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "openvpn-udp", "openvpn-tcp", "openvpn-https-tcp" ], "ingress_with_self": [ "all-all" ] } ], "oracle-db": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "oracle-db-tcp" ], "ingress_with_self": [ "all-all" ] } ], "postgresql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "postgresql-tcp" ], "ingress_with_self": [ "all-all" ] } ], "puppet": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "puppet-tcp", "puppetdb-tcp" ], "ingress_with_self": [ "all-all" ] } ], "rdp": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "rdp-tcp", "rdp-udp" ], "ingress_with_self": [ "all-all" ] } ], "redis": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "redis-tcp" ], "ingress_with_self": [ "all-all" ] } ], "redshift": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "redshift-tcp" ], "ingress_with_self": [ "all-all" ] } ], "splunk": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "splunk-indexer-tcp", "splunk-clients-tcp", "splunk-splunkd-tcp" ], "ingress_with_self": [ "all-all" ] } ], "squid": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "squid-proxy-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ssh": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ssh-tcp" ], "ingress_with_self": [ "all-all" ] } ], "storm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "storm-nimbus-tcp", "storm-ui-tcp", "storm-supervisor-tcp" ], "ingress_with_self": [ "all-all" ] } ], "web": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "web-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ], "winrm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "winrm-http-tcp", "winrm-https-tcp" ], "ingress_with_self": [ "all-all" ] } ], "zipkin": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "zipkin-admin-tcp", "zipkin-admin-query-tcp", "zipkin-admin-web-tcp", "zipkin-query-tcp", "zipkin-web-tcp" ], "ingress_with_self": [ "all-all" ] } ], "zookeeper": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "zookeeper-2181-tcp", "zookeeper-2888-tcp", "zookeeper-3888-tcp", "zookeeper-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ] }` | no |
| computed\_egress\_rules | List of computed egress rules to create by name | list(string) | `[]` | no |
| computed\_egress\_with\_cidr\_blocks | List of computed egress rules to create where 'cidr_blocks' is used | list(map(string)) | `[]` | no |
| computed\_egress\_with\_ipv6\_cidr\_blocks | List of computed egress rules to create where 'ipv6_cidr_blocks' is used | list(map(string)) | `[]` | no |
| computed\_egress\_with\_self | List of computed egress rules to create where 'self' is defined | list(map(string)) | `[]` | no |
| computed\_egress\_with\_source\_security\_group\_id | List of computed egress rules to create where 'source_security_group_id' is used | list(map(string)) | `[]` | no |
| computed\_ingress\_rules | List of computed ingress rules to create by name | list(string) | `[]` | no |
| computed\_ingress\_with\_cidr\_blocks | List of computed ingress rules to create where 'cidr_blocks' is used | list(map(string)) | `[]` | no |
| computed\_ingress\_with\_ipv6\_cidr\_blocks | List of computed ingress rules to create where 'ipv6_cidr_blocks' is used | list(map(string)) | `[]` | no |
| computed\_ingress\_with\_self | List of computed ingress rules to create where 'self' is defined | list(map(string)) | `[]` | no |
| computed\_ingress\_with\_source\_security\_group\_id | List of computed ingress rules to create where 'source_security_group_id' is used | list(map(string)) | `[]` | no |
| create | Whether to create security group and all rules | bool | `"true"` | no |
| description | Description of security group | string | `"Security Group managed by Terraform"` | no |
| egress\_cidr\_blocks | List of IPv4 CIDR ranges to use on all egress rules | list(string) | `[ "0.0.0.0/0" ]` | no |
| egress\_ipv6\_cidr\_blocks | List of IPv6 CIDR ranges to use on all egress rules | list(string) | `[ "::/0" ]` | no |
| egress\_prefix\_list\_ids | List of prefix list IDs (for allowing access to VPC endpoints) to use on all egress rules | list(string) | `[]` | no |
| egress\_rules | List of egress rules to create by name | list(string) | `[]` | no |
| egress\_with\_cidr\_blocks | List of egress rules to create where 'cidr_blocks' is used | list(map(string)) | `[]` | no |
| egress\_with\_ipv6\_cidr\_blocks | List of egress rules to create where 'ipv6_cidr_blocks' is used | list(map(string)) | `[]` | no |
| egress\_with\_self | List of egress rules to create where 'self' is defined | list(map(string)) | `[]` | no |
| egress\_with\_source\_security\_group\_id | List of egress rules to create where 'source_security_group_id' is used | list(map(string)) | `[]` | no |
| ingress\_cidr\_blocks | List of IPv4 CIDR ranges to use on all ingress rules | list(string) | `[]` | no |
| ingress\_ipv6\_cidr\_blocks | List of IPv6 CIDR ranges to use on all ingress rules | list(string) | `[]` | no |
| ingress\_prefix\_list\_ids | List of prefix list IDs (for allowing access to VPC endpoints) to use on all ingress rules | list(string) | `[]` | no |
| ingress\_rules | List of ingress rules to create by name | list(string) | `[]` | no |
| ingress\_with\_cidr\_blocks | List of ingress rules to create where 'cidr_blocks' is used | list(map(string)) | `[]` | no |
| ingress\_with\_ipv6\_cidr\_blocks | List of ingress rules to create where 'ipv6_cidr_blocks' is used | list(map(string)) | `[]` | no |
| ingress\_with\_self | List of ingress rules to create where 'self' is defined | list(map(string)) | `[]` | no |
| ingress\_with\_source\_security\_group\_id | List of ingress rules to create where 'source_security_group_id' is used | list(map(string)) | `[]` | no |
| name | Name of security group | string | n/a | yes |
| number\_of\_computed\_egress\_rules | Number of computed egress rules to create by name | number | `"0"` | no |
| number\_of\_computed\_egress\_with\_cidr\_blocks | Number of computed egress rules to create where 'cidr_blocks' is used | number | `"0"` | no |
| number\_of\_computed\_egress\_with\_ipv6\_cidr\_blocks | Number of computed egress rules to create where 'ipv6_cidr_blocks' is used | number | `"0"` | no |
| number\_of\_computed\_egress\_with\_self | Number of computed egress rules to create where 'self' is defined | number | `"0"` | no |
| number\_of\_computed\_egress\_with\_source\_security\_group\_id | Number of computed egress rules to create where 'source_security_group_id' is used | number | `"0"` | no |
| number\_of\_computed\_ingress\_rules | Number of computed ingress rules to create by name | number | `"0"` | no |
| number\_of\_computed\_ingress\_with\_cidr\_blocks | Number of computed ingress rules to create where 'cidr_blocks' is used | number | `"0"` | no |
| number\_of\_computed\_ingress\_with\_ipv6\_cidr\_blocks | Number of computed ingress rules to create where 'ipv6_cidr_blocks' is used | number | `"0"` | no |
| number\_of\_computed\_ingress\_with\_self | Number of computed ingress rules to create where 'self' is defined | number | `"0"` | no |
| number\_of\_computed\_ingress\_with\_source\_security\_group\_id | Number of computed ingress rules to create where 'source_security_group_id' is used | number | `"0"` | no |
| rules | Map of known security group rules (define as 'name' = ['from port', 'to port', 'protocol', 'description']) | map(list(any)) | `{ "carbon-relay-ng": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "carbon-line-in-tcp", "carbon-line-in-udp", "carbon-pickle-tcp", "carbon-pickle-udp", "carbon-gui-udp" ], "ingress_with_self": [ "all-all" ] } ], "cassandra": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "cassandra-clients-tcp", "cassandra-thrift-clients-tcp", "cassandra-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ], "consul": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "consul-tcp", "consul-cli-rpc-tcp", "consul-webui-tcp", "consul-dns-tcp", "consul-dns-udp", "consul-serf-lan-tcp", "consul-serf-lan-udp", "consul-serf-wan-tcp", "consul-serf-wan-udp" ], "ingress_with_self": [ "all-all" ] } ], "docker-swarm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "docker-swarm-mngmt-tcp", "docker-swarm-node-tcp", "docker-swarm-node-udp", "docker-swarm-overlay-udp" ], "ingress_with_self": [ "all-all" ] } ], "elasticsearch": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "elasticsearch-rest-tcp", "elasticsearch-java-tcp" ], "ingress_with_self": [ "all-all" ] } ], "http-80": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "http-80-tcp" ], "ingress_with_self": [ "all-all" ] } ], "https-443": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "https-443-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ipsec-4500": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ipsec-4500-udp" ], "ingress_with_self": [ "all-all" ] } ], "ipsec-500": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ipsec-500-udp" ], "ingress_with_self": [ "all-all" ] } ], "kafka": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "kafka-broker-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ldaps": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ldaps-tcp" ], "ingress_with_self": [ "all-all" ] } ], "memcached": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "memcached-tcp" ], "ingress_with_self": [ "all-all" ] } ], "mssql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "mssql-tcp", "mssql-udp", "mssql-analytics-tcp", "mssql-broker-tcp" ], "ingress_with_self": [ "all-all" ] } ], "mysql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "mysql-tcp" ], "ingress_with_self": [ "all-all" ] } ], "nfs": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "nfs-tcp" ], "ingress_with_self": [ "all-all" ] } ], "nomad": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "nomad-http-tcp", "nomad-rpc-tcp", "nomad-serf-tcp", "nomad-serf-udp" ], "ingress_with_self": [ "all-all" ] } ], "openvpn": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "openvpn-udp", "openvpn-tcp", "openvpn-https-tcp" ], "ingress_with_self": [ "all-all" ] } ], "oracle-db": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "oracle-db-tcp" ], "ingress_with_self": [ "all-all" ] } ], "postgresql": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "postgresql-tcp" ], "ingress_with_self": [ "all-all" ] } ], "puppet": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "puppet-tcp", "puppetdb-tcp" ], "ingress_with_self": [ "all-all" ] } ], "rdp": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "rdp-tcp", "rdp-udp" ], "ingress_with_self": [ "all-all" ] } ], "redis": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "redis-tcp" ], "ingress_with_self": [ "all-all" ] } ], "redshift": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "redshift-tcp" ], "ingress_with_self": [ "all-all" ] } ], "splunk": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "splunk-indexer-tcp", "splunk-clients-tcp", "splunk-splunkd-tcp" ], "ingress_with_self": [ "all-all" ] } ], "squid": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "squid-proxy-tcp" ], "ingress_with_self": [ "all-all" ] } ], "ssh": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "ssh-tcp" ], "ingress_with_self": [ "all-all" ] } ], "storm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "storm-nimbus-tcp", "storm-ui-tcp", "storm-supervisor-tcp" ], "ingress_with_self": [ "all-all" ] } ], "web": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "web-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ], "winrm": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "winrm-http-tcp", "winrm-https-tcp" ], "ingress_with_self": [ "all-all" ] } ], "zipkin": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "zipkin-admin-tcp", "zipkin-admin-query-tcp", "zipkin-admin-web-tcp", "zipkin-query-tcp", "zipkin-web-tcp" ], "ingress_with_self": [ "all-all" ] } ], "zookeeper": [ { "egress_rules": [ "all-all" ], "ingress_rules": [ "zookeeper-2181-tcp", "zookeeper-2888-tcp", "zookeeper-3888-tcp", "zookeeper-jmx-tcp" ], "ingress_with_self": [ "all-all" ] } ] }` | no |
| tags | A mapping of tags to assign to security group | map(string) | `{}` | no |
| use\_name\_prefix | Whether to use name_prefix or fixed name. Should be true to able to update security group name after initial creation | bool | `"true"` | no |
| vpc\_id | ID of the VPC where to create security group | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| this\_security\_group\_description | The description of the security group |
| this\_security\_group\_id | The ID of the security group |
| this\_security\_group\_name | The name of the security group |
| this\_security\_group\_owner\_id | The owner ID |
| this\_security\_group\_vpc\_id | The VPC ID |
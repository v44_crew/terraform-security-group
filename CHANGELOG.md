## v1.0.1 - 2019-09-05

- Update pipeline 
- Update README.md

## v1.0.0 - 2019-08-27

- Initial module structure
- Initial Pipeline